This document will describe all the tests that we do in QA in order to test the ledger app:

So the thinking here is there are 2 types of needs we need to test for,
firstly security, meaning no one can do anything malicious because of a bug, and
secondly usability, meaning there are no hiccups in the usability of the product.
I want to state that security is the most important feature, because whilst usability bugs might be
embarrassing, they are far less embarrassing then someone losing money because of a vulnerability created by a bug.

Lets start thinking about security, the only things that are valuable to an attacker is getting public keys,
and signing an unauthorised transaction.

Getting public keys maliciously with a bug can happen in a few ways:

1] Getting public keys without authentication
2] Getting public keys after authentication but a different key is sent
3] Getting public keys with declined authentication

Another malicious thing an attacker might want to do is sign an transaction withouth authorization,
in order to do that he needs to load the txn hash into state, meaning calling auth txn hash:

4] Being able to sign a txn even though it was declined
5] Sending a txn with 0 outputs so that authorization won't be asked for
6] Adding data after a txn was already fully authorized

Also in order to protect clumsy users, check the same things we checked for 
in get public key

7] Loading txn without authentication
8] Loading a txn after authentication but a different key is sent
9] Loading a txn with declined authentication

The next thing we're testing for is checking all code paths, a bug in a certian code path might allow an attacker to do all sorts of unwanted stuff with the app.
Since doing all code paths is a trublsum job, we're trying to test most code paths here:

10] Authorize txns that check all of the cases of parsing functions:
	readVarIntUncommitted  -
								1 byte case - input #0 in txn4
								2 byte case - input #1 in txn4
	readVarIntFromBuffer   -
								1 byte case - txn4 output #4 - in the contract version
								2 byte case - txn4 output #5 - in the contract version
   	readByteUncommitted    - 	only 1 case and gets tested in all of the txns
   	readamout 			   -	all of the cases are checked in txn4 outputs #0,3,9,13,16,17

   	readAssetUncommitted   - 	1 byte version - txn4 output #9
   								2 byte version - txn4 output #10
   								3 byte version - txn4 output #11
   								4 byte version - txn4 output #12

   								testCaseIfed 10 - txn4 output #9
   								testCaseIfed 11 - txn4 output #9
   								testCaseIfed 12 - txn4 output #12

   	amountToString         - 	
   								case for a float with dot somethign 				like 100.0001	- txn4 output #4
   								case for a float without a dot 						like 100		- txn4 output #3
   								case for a float wihtout a number after the float 	like 0.001		- txn4 output #0
   								none float mode 									like 100		- txn4 output #9

 	contractIdToBech32     - 	1 cases  						txn4 output #4 
							contract should be czen1qqqqqqqy25404hk5ny5edl842xct2qnw75padg4ch9vdv72m4mr9kvzc0r5a9hwjl 
   	
   	parseBuffer            - 	version != 0 					txn5
                                0 outputs						txn2
                                fee locks						txn4 output #9
                                pklock							txn4 output #0
                                activation sacrafice			txn4 output #3
                                send to contract 				txn4 output #4
                                extention sacrafice				txn4 output #22
                                coinbase error					txn6 output #0
                                destry Lock  					txn4 output #8
 	                          	unkwon locks  					txn7 output #0
   								zp assest and none zp assest	txn4 output #0 and output #9
   								with contract and without 		txn4 with contract and tx1 without a contract
	bech32Encode           - 									txn4 output #0
	handleAuth			   -	auth code is not null terminated

	(Note: There are some functions for which we're not checking all code paths)

This means you have to test:
	A] txn 5 fails because of the wrong version RET_VAL_PARSE_BUFFER_BAD_VERSION_ERR = 11
	B] txn 7 fails because of unknown locks RET_VAL_PARSE_BUFFER_UNKNOWN_LOCK_ERR = 14
	C] txn 6 fails because of no support for coinbase locks RET_VAL_PARSE_BUFFER_UNSUPPORTED_OUTPUT_ERR = 13
	D] txn 1 should pass (no need to test, tested in test 12)
	E] txn 4 should pass (no need to test, tested in test 12)
	F] txn 2 fails because of 0 outputs RET_VAL_PARSE_BUFFER_ZERO_OUTPUTS_ERR = 26 (no need to do this, it's already tested in test 5)
	G] test 11 tests for wrong auth code termintor

-------------------

So after we checked for potential security issues, and code paths, we need to check for usability and stress. This means that the public keys generated are correct, signing works and the signitures are vaild, redoing authorization between tests doesn't mess anything up and usability still works after declining a txn output.

11] Check that the build works without warnings
12] Check if getVersion is valid
13] Check if getpublickey works after auth and check the key against a wallet
14] Sign txn4 (the most complicated one) with 3 keys (1 different on each parameter) and make sure the sigs are correct
15] Do another Auth
16] Check if getpublickey works
17] Authorize a simple txn and make sure the 1 sig is correct
18] Try to auth some simple txn and decline
19] Try to call auth on some simple txn and make sure the sig is good

Test 1:
	
	0] Reset the app
	1] Run getpublickey and make sure you get a no auth error (RET_VAL_NOT_AUTHENTICATED_ERR = 4)

Test 2:

	0] Do Auth
	1] Swap the auth key
	2] Call get public key
	3] Make sure you get wrong key error #RET_VAL_BAD_AUTH_KEY_ERR = 5

Test 3:
	
	0] Do Auth
	1] Decline
	2] Try to call get publickeys
	3] Make sure you get (RET_VAL_NOT_AUTHENTICATED_ERR = 4)

Test 4:

	0] Do auth
	1] Try to auth txn #1 (all at once) and decline
	2] Try to sign it
	3] Make sure you get an error

Test 5:

	0] Do auth
	1] Send a Txn with 0 outputs
	2] Make sure you get an error and cant sign


Test 6:

	0] Do auth
	1] Send a txn with some extra bits at the end (tx3), bit by bit, autherize the outputs, 
	and make sure you get an error    RET_VAL_PARSE_BUFFER_NOT_ALL_BYTES_CONSUMED = 18


Test 7:

	0] Reset the app
	1] Try to load any txn
	2] Make sure you get a no auth error  |   RET_VAL_NOT_AUTHENTICATED_ERR = 4

Test 8:

	0] do Auth
	1] Change the key
	2] Try to load any txn
	3] Make sure you get a bad key error | 	RET_VAL_BAD_AUTH_KEY_ERR = 5

Test 9:

	0] do Auth and decline
	1] Try to load any txn
	2] Make sure you get an errors		|    RET_VAL_NOT_AUTHENTICATED_ERR = 4

Test 10 A:

	0] do Auth
	1] Try to load txn #5 and get | RET_VAL_PARSE_BUFFER_BAD_VERSION_ERR = 11
	
Test 10 B:

	0] do Auth
	1] Try to laod txn #7 and fail because of unknown locks RET_VAL_PARSE_BUFFER_UNKNOWN_LOCK_ERR = 14

Test 10 C:

	0] do Auth
	1] txn 6 fails because of no support for coinbase locks RET_VAL_PARSE_BUFFER_UNSUPPORTED_OUTPUT_ERR = 13

Test 11:

	0] run make clean
	1] run make

	Check that there are no errors or warnings coming out of files in our repo

Test 12 - 18:

	0] Call check version, make sure 1 and 'Zen2Moon' is returned

	1] Do auth

	2] Call get public key, make sure the key is the same as in the wallet or desktop app with the same seed
	(zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6 for the testing seed used in the office)
	(testing seed: https://docs.google.com/document/d/1d4iEA1ahqL37NW3jFN-FpBNxlOYGiSzYb0ygDIYSlnw/edit?usp=sharing)

	3] Do auth
	4] Send the txn #4, which invocates all the types of locks and most code paths
	
	Outputs exactly should match:
		1] 0.0001 ZP to zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6
		2] sacrifice 100 ZP
		3] 10.00001 ZP to czen1qqqqqqqy25404hk5ny5edl842xct2qnw75padg4ch9vdv72m4mr9kvzc0r5a9hwjl (it's a vailed contract in the block explorer)
		4] 0.0000001 ZP to czen1qqqqqrla2hw4th24m42a64wa2hw4th24m42a64wa2hw4th24m42a64wa2hv9wmegv (caluclated using python bech32 encode)
		5] 0.0000001 ZP to czen1qqqqrlla2hw4th24m42a64wa2hw4th24m42a64wa2hw4th24m42a64wa2hv6w944g (caluclated using python bech32 encode)
		6] 0.0000001 ZP to czen1qq8lllla2hw4th24m42a64wa2hw4th24m42a64wa2hw4th24m42a64wa2hvynpamy (caluclated using python bech32 encode)
		7] destroy 0.00000012 ZP
		8] fee 100 of asset 00000000aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc0000
		9] fee 12345678 of asset 000001ffaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc1100
		10] fee 12345678 of asset 00003fffaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc
		11] fee 12345678 of asset 01ffffffaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc
		12] fee 0.67108865 ZP
		13] fee 0.50331647 ZP
		14] fee 0.00000511 ZP
		15] fee 720575940.37927921 ZP
		16] fee 184467440737.09551601 ZP
		17] extention sacrifce of 0.00123123 ZP to czen1qr6yn5wcqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqxt20q7 (caluclated using python bech32 encode)

	5] Get the public keys for the coresponding indexes (0,0,0) , (1,0,0) , (0,1,0)

	6] Sign sign with the keys (0,0,0) , (1,0,0) , (0,1,0) and check the sigs are correct for the public keys generated above, with the F# code at the end of this file

	7] Do auth again (make sure a different key is called here)

	8] Check that getpublickey still works
	
	9] Autherize simple txn #1: should be sending to: zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6 and make sure the sig is valid

	10] Try to auth the simple txn #1 but decline the first output

	11] Autherize simple txn #1
	12] Sign with (0, 0, 0) and make sure sig works


todo: test for auth code bigger then 6 letters

Test Txns:
	
Txn #1:

(Just a simple txn with 1 output and no contract)

Should send to wallet 0,0,0: zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6

00000000010100000000000000000000000000000000000000000000000000000000000000000001022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00040100

let tx:Transaction = {
    version = 0ul
    inputs = [Types.Outpoint {txHash = Hash <| Array.zeroCreate<byte> 32; index = 0ul}]
    outputs = [{lock=Types.Lock.PK ("43e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd" |> Base16.decode  |> Option.get |> Hash); spend={asset= Asset.Zen; amount=10UL}};]
    witnesses = []
    //Tests No contract
    contract = None
}

Txn #2:

(Just a simple txn with 0 outputs, it should fail)

0000000001010000000000000000000000000000000000000000000000000000000000000000000000

let tx:Transaction = {
    version = 0ul
    inputs = [Types.Outpoint {txHash = Hash <| Array.zeroCreate<byte> 32; index = 0ul}] //todo play with this
    outputs = []
    witnesses = []
    contract = None
}

Txn #3:

Txn1 but with some extra bytes at the end

00000000010100000000000000000000000000000000000000000000000000000000000000000001022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd0004010000000000

Txn #4:

    let tx:Transaction = {
        version = 0ul
        inputs = [Types.Outpoint {txHash = Hash <| Array.zeroCreate<byte> 32; index = 0ul};   //Checking case 1 byte for readVarIntUncommitted here
                  Types.Outpoint {txHash = Hash <| Array.zeroCreate<byte> 32; index = 300ul}] //Checking case 2 bytes for readVarIntUncommitted here
        outputs = [
                   //#0 checking amountToString should be 0.001 and pklock and bech32encode and ZPAsset and testcasedIfed(4) 
                   {lock=Types.Lock.PK ("43e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd" |> Base16.decode  |> Option.get |> Hash); spend={asset= Asset.Zen; amount=10000UL}};

                   //#1 The longest string you can make explaining a txn
                   //{lock=Types.Lock.Contract (ContractId (0b11111111111111111111111111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaa11"  |> Base16.decode  |> Option.get |> Hash)); spend={asset=Asset ((ContractId (0b11111111111111111111111111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)), "11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11aa"  |> Base16.decode  |> Option.get |> Hash); amount=12345678912345678912UL}};
                   
                   //#3 checking amountToString should be 100 and activation sacrifice lock
                   {lock=Types.Lock.ActivationSacrifice; spend={asset= Asset.Zen; amount=10000000000UL}};
                   
                   //the lock bellow should corespond to this address: zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6
                   
                   //#4 checking amountToString to be 100.0001 and contractIdToBech32 (should be czen1qqqqqqqy25404hk5ny5edl842xct2qnw75padg4ch9vdv72m4mr9kvzc0r5a9hwjl) and sendToContract lock and testcaseIfed(5)
                   {lock=Types.Lock.Contract (ContractId (0b00000000ul,  "8aa55f5bda932532df9eaa3616a04ddea07ad457172b1acf2b75d8cb660b0f1d"  |> Base16.decode  |> Option.get |> Hash)); spend={asset= Asset.Zen; amount=1000001000UL}};
                   
                   //#5
                   {lock=Types.Lock.Contract (ContractId (0b111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)); spend={asset= Asset.Zen; amount=10UL}};
                   //#6
                   {lock=Types.Lock.Contract (ContractId (0b11111111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)); spend={asset= Asset.Zen; amount=10UL}};
                   //#7
                   {lock=Types.Lock.Contract (ContractId (0b00000001111111111111111111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)); spend={asset= Asset.Zen; amount=10UL}};
                   
                   //#8
                   {lock=Types.Lock.Destroy; spend={asset= Asset.Zen; amount=12UL}};
                   
                   //#9 checking amountToString for 100 and fee lock and noZp asset and 1 byte asset version and testcaseIfed(10) for readAssetUncommited and testcaseIfed(11) for readAssetUncommited
                   {lock=Types.Lock.Fee; spend={asset=Asset ((ContractId (0b00000000ul,"aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)), "11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc0000"  |> Base16.decode  |> Option.get |> Hash); amount=100UL}};
                   
                   //#10 2 byte asset version and testCaseIfeded
                   {lock=Types.Lock.Fee; spend={asset=Asset ((ContractId (0b111111111ul,"aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)), "11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc1100"  |> Base16.decode  |> Option.get |> Hash); amount=12345678UL}};
                   
                   //#11 3 byte asset version and testcasedIfed(12)
                   {lock=Types.Lock.Fee; spend={asset=Asset ((ContractId (0b000011111111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)), "11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc"  |> Base16.decode  |> Option.get |> Hash); amount=12345678UL}};
                   
                   //#12 4 byte asset version
                   {lock=Types.Lock.Fee; spend={asset=Asset ((ContractId (0b00000001111111111111111111111111ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get |> Hash)), "11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc"  |> Base16.decode  |> Option.get |> Hash); amount=12345678UL}};
                   
                   //#13  testcaseIfed(6)
                   {lock=Types.Lock.Fee; spend={asset= Asset.Zen; amount=67108865UL}};
                   
                   //#14  
                   {lock=Types.Lock.Fee; spend={asset= Asset.Zen; amount=0x02FFFFFFUL}};
                   
                   //#15
                   {lock=Types.Lock.Fee; spend={asset= Asset.Zen; amount=0x01FFUL}};
                   
                   //#16 testCaseIfed(2) - works
                    {lock=Types.Lock.Fee; spend={asset= Asset.Zen; amount=0x00FFFFFFFFFFFFF1UL}};
                    
                   //#17 testCaseIfed(1) - works
                    {lock=Types.Lock.Fee; spend={asset= Asset.Zen; amount=0xFFFFFFFFFFFFFFF1UL}};
                    
                    //#22 - checking extention sacrafice
                   {lock=Types.Lock.ExtensionSacrifice (ContractId(512309819ul, Hash <| Array.zeroCreate<byte> 32)); spend={asset= Asset.Zen; amount=123123UL}}
                    ]
        witnesses = []
        contract = Some (HighV (1234567178ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb"  |> Base16.decode  |> Option.get));
    }
    
    printfn " Result : %A" (Transaction.toHex tx)

000000000201000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000812c11022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00100103000028010421008aa55f5bda932532df9eaa3616a04ddea07ad457172b1acf2b75d8cb660b0f1d008c0f42410422827faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb0004010422fe7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb00040104248efefe7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb000401070000000c010040aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb1e11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc08010100e37faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc110080bc614e0100e0ff7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc80bc614e0100efffff7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc80bc614e010000c000000101000082ffffff01000001ff0100007efffffffffffff1010000fefffffffffffffff1052580f3a3f33b0000000000000000000000000000000000000000000000000000000000000000008001e0f30183cbd6ff0a8358aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb

Txn #5:

Tx1 but with different version, at the begining

ddee0000010100000000000000000000000000000000000000000000000000000000000000000001022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd0004010000000000

Txn #6:

Test that coinbase locks error

let tx:Transaction = {
    version = 0ul
    inputs = [Types.Outpoint {txHash = Hash <| Array.zeroCreate<byte> 32; index = 0ul}] //todo play with this
    
    //tests coinbase lock errors
    outputs = [{lock=Types.Lock.Coinbase (2102ul, "aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb" |> Base16.decode |> Option.get |> Hash); spend={asset= Asset.Zen; amount=123UL}}]
    witnesses = []
    contract = None
}

00000000010100000000000000000000000000000000000000000000000000000000000000000001062400000836aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb00007b00

Txn #7:

A copy for Txn1 with the lock changed to 0x1E that dont exist (warning, the msb of that byte can't be 1 because everything there is read as varint)

000000000101000000000000000000000000000000000000000000000000000000000000000000011e2043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00040100

F# Code to check sigs:

[<Test>]
let ``sigTest``() =
    let tx = "000000000201000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000812c11022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00100103000028010421008aa55f5bda932532df9eaa3616a04ddea07ad457172b1acf2b75d8cb660b0f1d008c0f42410422827faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb0004010422fe7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb00040104248efefe7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb000401070000000c010040aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb1e11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc08010100e37faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc110080bc614e0100e0ff7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc80bc614e0100efffff7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc80bc614e010000c000000101000082ffffff01000001ff0100007efffffffffffff1010000fefffffffffffffff1052580f3a3f33b0000000000000000000000000000000000000000000000000000000000000000008001e0f30183cbd6ff0a8358aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb" 
    
    let txHash = Base16.decode tx
                 |> Option.get
                 |> Hash.compute
    
    
    //let sigEncoded =      "3045022100f4d6b211f23f1797d1411a5e31ca6f907f77cf8d47828815d9ef4e9f42428bcd02200e40683ea0142e5ac0150fc81d034cab8e42d80942e43f4731b8bc1ad33bdd"
    let sigEncoded = "5610af11b2f4ab1a86d067586659076147381dc70c1b1a90ce67978b6e44d77911b869a80799e17ad4c73eb433dc75c17478cf6e967d9072141d044633ff4054"
    
    printfn "asdasd"
    
    let sign = Signature.fromString sigEncoded |> Option.get
    
    let pubKeyEncoded = "02fbb686e35e00ad397f2fe79df82b8e175a79a46bef9f73a13a68152cefaa27b4"
    
    printfn "twotwo"
    
    let pubKey = Base16.decode pubKeyEncoded
                 |> Option.get
                 |> PublicKey.deserialize |> Option.get
    
    
    printfn "%A" <| txHash
    
    printfn "%A" <| pubKey
    
    printfn "%A" <| sign

    printfn "%A" <| Crypto.verify pubKey sign txHash
