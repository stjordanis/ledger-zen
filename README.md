# nanos-app-zen

This is the official Zen Protocol wallet app for the Ledger Nano S.

Features include:

* Get version - This function returns the ledger app version along with the ledger app magic (it's a buffer), it's used to check whether or not the zen app running on a ledger is connected to the computer and if the version is fitting

* Client to ledger authentication - The ledger shows an authentication 5 letter text which is also displayed on the screen of the client. In this exchange an authentication api key is also exchanged, this key is then used to authenticate all commands from the client to the ledger.
This security feature was made so that unwanted browser pages or apps won't be able to connect and communicate with your ledger without your authentication. For example an unwanted scenario would be a hidden browser window stealing your Zen public keys, since any browser window can communicate with your app once connected. This vulnerability exists today in all other ledger apps AFAIK.

* Get public keys - The ledger generates public keys it can later sign txn hashes
for, the keys are generated from full bip 32 Zen Protocol paths (44'/258), you can ask for any cointype, account number and address index. Keys are generated in the amounts of 7 at a time (address index + 0, address index + 1, ... , address index + 6)

* Load Txn Hash - This allows the client to load a txn into the ledger, it can later ask to sign for with any one of the keys that can be derived by the ledger's seed and Zen's bip 32 path.
The ADPU protocol only supports 256 byte buffers, so bigger transactions have to be loaded part by part. During the loading process, all of the outputs of the transaction are authenticated by the user

* Sign a loaded Txn Hash - Assuming you have a txn already authenticated, you can ask to sign it with any one of the keys owned by that ledger.

## Code design:

The try/catch wrapped main handler loop is in main.c, it calls the blocking function io_exchange waiting for the next APDU and then calls the appropriate command handler, according to the instructed command.

## APDU Command Structure

Incoming APDU commands come in the form of:
	
	0xE0 <commandId byte> <param1 byte> <param2 byte> <dataSize byte> <data buffer>

In most commands the start of the data buffer is the 32 byte authentication key

Outgoing APDU commands:

	<ret status byte> <buffer> <0x90> <0x00>

You can find all the different ret values and statuses in retEnums.h

## The IO_EXCHANGE flag:

When sending sending back APDU commands using IO_EXCHANGE, the first parameter gives you the option to light up 2 flags: IO_RETURN_AFTER_TX and IO_ASYNCH_REPLY, I don't really understand the difference and how they are supposed to be used, but the developer of the SIA app explains it well here: https://github.com/NebulousLabs/nanos-app-sia/blob/7e0706e19ebf840ad78756d88926d34c10cd37d1/src/calcTxnHash.c#L342

## Include style

In this implementation I went with a periculer include style, which states, H files don't include
anything, C files have to include all of their dependencies. It just makes life much easier.

## How to compile

For people from the Zen Team there is a Gitlab issue, explaining where the build machine image is located

The compile command is:
	make
In order to compile + upload an App to the ledger you call:
	make load

## Python handler lib

To communicate with the ledger you can use the ledgerZenApp.py class, the functions are pretty much self explanatory.

MainRunTest.py has a few tests that use the py app so you can get examples from there

## How to test

Before releasing a version there is a list of tests you should run, described in QATests.txt, most of tests there are implemented in mainRunTest.py

If you want to test bech32 encodings, meaning checking out how they are supposed to look like, you can use the python file bech32.py
Also for creating custom txns for testing you can use the F# code found at the end of QATests.txt (you can create a F# test out of that code), for testing signiture outputs there is a F# test at the end of QATests.txt that you can use 

## Debugging

There is a new debugging bolos OS FW version built that wasn't around when whilst the app was developed, so 2 functions where made:

	debugSend() - acting like a breakpoint that you can't continue from.
It immediately sends an APDU from your app, with it you can make sure code executed at the breakpoint, and get 2 1 byte variables, 2 2 byte variables and a buffer back from the code.

testCaseIfed - This is debugSend wrapped inside an if, a conditional breakpoint. When calling the function authTxnHash you pass in debugCase as the second parameter byte, which is checked against in testCaseIfed, meaning you pick which conditional breakpoint to activate.It's especially useful for testing functions that have multiple cases which get invoked in the same txn, like readAmount()

## How to change the logo of the app

Just replace nanos_app_zen.gif with the new logo

## Txn Protocol

write_Transaction_Full(inputs, outputs, optional_contract, witnesses) = 

4bytes of version
write_Seq(inputs):<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if output:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0x1, write_Hash(txHash); write_VarInt(index);<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if mint:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0x2; write_Asset(asset); write_Amount(amount)<br/><br/>
<br/>
write_Seq(outputs):<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if feelock:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x1); write_bytes_variable([])<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elseif pklock:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x2); write_Bytes_Variable(sha256_3(publickey));<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elseif sacrifice:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x3); write_bytes_variable([])<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elseif contract lock:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x4); write_Bytes_Variable(write_VarInt(version); write_Hash(hash));<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elseif extention sacrifice:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x5); write_Bytes_Variable(write_VarInt(version); write_Hash(hash));<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elseif coinbase:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x6); write_Bytes_Variable(32bytehash + 32bytehash);<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elseif coinbase:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x7); write_Bytes_Variable([])<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(version); write_Bytes_Variable(payload);<br/>
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_Asset(asset); write_Amount(amount)<br/>
<br/>
if not optional_contract;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0x0;<br/>
else:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0x1; write_VarInt(version); write_Byte_Array_Variable(payload)<br/>
<br/>
write_Seq(witnesses).<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if PKWitness:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(0x1); write_Bytes_Variable(0x1; public key, libspec256k1Sing(sha256_3(txhash)))<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;write_VarInt(identifier); write_Bytes_Variable(payload)

