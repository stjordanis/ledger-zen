from ledgerblue.comm import getDongle
from ledgerblue.commException import CommException
import argparse
import struct
from decimal import Decimal
import hashlib
import base58
import bech32
import sha3
import random
import time
import binascii
from ledgerZenApp import ledgerZenApp

tx1 = "00000000010100000000000000000000000000000000000000000000000000000000000000000001022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00040100".decode('hex')
tx2 = "0000000001010000000000000000000000000000000000000000000000000000000000000000000000".decode('hex')
tx3 = "00000000010100000000000000000000000000000000000000000000000000000000000000000001022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd0004010000000000".decode('hex')
tx4 = "000000000201000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000000812c11022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00100103000028010421008aa55f5bda932532df9eaa3616a04ddea07ad457172b1acf2b75d8cb660b0f1d008c0f42410422827faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb0004010422fe7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb00040104248efefe7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb000401070000000c010040aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb1e11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc08010100e37faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc110080bc614e0100e0ff7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc80bc614e0100efffff7faabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc11cc80bc614e010000c000000101000082ffffff01000001ff0100007efffffffffffff1010000fefffffffffffffff1052580f3a3f33b0000000000000000000000000000000000000000000000000000000000000000008001e0f30183cbd6ff0a8358aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb".decode('hex')
tx5 = "ddee0000010100000000000000000000000000000000000000000000000000000000000000000001022043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd0004010000000000".decode('hex')
tx6 = "00000000010100000000000000000000000000000000000000000000000000000000000000000001062400000836aabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabbaabb00007b00".decode('hex')
tx7 = "000000000101000000000000000000000000000000000000000000000000000000000000000000011e2043e7f9c2590d0542aacd2822bf27eb719b1e3c012302f2c9ce885e909b41d8fd00040100".decode('hex')

zen = ledgerZenApp()

test = 11

if 1 == test:

	print(zen.getPublicKeys(0, 0, 0)) #(RET_VAL_NOT_AUTHENTICATED_ERR = 4)

if 2 == test:

	zen.doAuth("test2")
	zen.changeKey()
	zen.getPublicKeys(0, 0, 0) #RET_VAL_BAD_AUTH_KEY_ERR = 5,

if 3 == test:

	zen.doAuth("decli")
	zen.getPublicKeys(0, 0, 0) #(RET_VAL_NOT_AUTHENTICATED_ERR = 4)


if 4 == test:
	zen.doAuth("accep")
	zen.calcTxHash(tx1, 0, 200)
	zen.signTxn(0, 0, 0) # RET_VAL_TXN_NOT_AUTHORIZED_ERR = 8

if 5 == test:
	zen.doAuth("accep")
	zen.calcTxHash(tx2, 0, 200) # RET_VAL_PARSE_BUFFER_ZERO_OUTPUTS_ERR

if 6 == test:
	zen.doAuth("accep")
	zen.calcTxHash(tx1, 0, 1)
	zen.calcTxHash("000000".decode('hex'), 0, 1)   #RET_VAL_PARSE_BUFFER_NOT_ALL_BYTES_CONSUMED = 18

if 7 == test:
	#make sure to reset the app
	zen.calcTxHash(tx1, 0, 1)   	#RET_VAL_NOT_AUTHENTICATED_ERR = 4

if 8 == test:

	zen.doAuth("accep")
	zen.changeKey()
	zen.calcTxHash(tx1, 0, 1) 		#RET_VAL_BAD_AUTH_KEY_ERR = 5

if 9 == test:

	zen.doAuth("decli")
	zen.calcTxHash(tx1, 0, 1)		#RET_VAL_NOT_AUTHENTICATED_ERR = 4

if 100 == test: #10 A

	zen.doAuth("accep")
	zen.calcTxHash(tx5, 0, 200) 	# RET_VAL_PARSE_BUFFER_BAD_VERSION_ERR = 11

if 101 == test: #10 B

	zen.doAuth("accep")
	zen.calcTxHash(tx7, 0, 200) 	#RET_VAL_PARSE_BUFFER_UNKNOWN_LOCK_ERR = 14

if 102 == test: #10 C

	zen.doAuth("accep")
	zen.calcTxHash(tx6, 0, 200)     #RET_VAL_PARSE_BUFFER_UNSUPPORTED_OUTPUT_ERR = 13

if 11 == test:

	zen.doAuth("accep", 1) 			#RET_VAL_WRONG_SIZE_AUTH_CODE

if 12 == test:

	print(zen.getVersion(1)) # make sure 1 is returned, also with the zen sig

	zen.doAuth('accep')

	print(zen.getPublicKeys(0, 0, 0)[0][2]) #make sure the key is zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6

	zen.changeKey()
	zen.doAuth('accep')

	zen.calcTxHash(tx4, 0, 1)

	print(zen.getPublicKeys(0, 0, 0)[0][0]) #035909da9f3e56c56b17d9b6c7aab8fe547b4ce1d91494268cfbc06786656ccde1
	print(zen.getPublicKeys(0, 1, 0)[0][0]) #025fe3f8c7fb709c911b845ec4a1538b5407b4bcfcc601d991f03dffce7d277d48
	print(zen.getPublicKeys(1, 0, 0)[0][0]) #02fbb686e35e00ad397f2fe79df82b8e175a79a46bef9f73a13a68152cefaa27b4

	print(zen.signTxn(0, 0, 0))  #this sig is valid and was checked via F# code: {'s': 'ba9cf656cc7ad206042c46b7d1b3cd2d38ec995bee35aefe0adc7211a8e13d08', 'r': '7e715842534aebff45756d96f28297c05e9498eeeb613e691534583bb48a9fa6'}
	print(zen.signTxn(0, 1, 0))  #this sig is valid and was checked via F# code: {'s': 'dcf8b601c1f17dad20c97ba3ae47f8f98817f415ddb8b379e763e044a2213f52', 'r': '7dd598c38916b53ab7afae0ee437c944ff724e399f14973edae00681480cba64'}
	print(zen.signTxn(1, 0, 0))  #this sig is valid and was checked via F# code: {'s': '5610af11b2f4ab1a86d067586659076147381dc70c1b1a90ce67978b6e44d779', 'r': '11b869a80799e17ad4c73eb433dc75c17478cf6e967d9072141d044633ff4054'}

	zen.changeKey()
	zen.doAuth('accep')

	print(zen.getPublicKeys(0, 0, 0)[0][2]) #make sure the key is zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6

	zen.calcTxHash(tx1, 0, 1) #check simple txn sending addr should be zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6
	print(zen.signTxn(0, 0, 0))  #this sig is valid and was checked via F# code: {'s': '0dda044ee7132f145c8fc0f73a3db5c6e5cf09422716eeddfe457b7741d432ba', 'r': '41e7d34d699902391c446cc4e98cac323f5a891c2b893bc666e240f8948e4327'}

	zen.calcTxHash(tx1, 0, 1) #decline here  #should be sending to zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6

	print("accept the next one")

	zen.calcTxHash(tx1, 0, 1) #accept this #should be sending to zen1qg0nlnsjep5z592kd9q3t7fltwxd3u0qpyvp09jww3p0fpx6pmr7s0ukva6

	print(zen.signTxn(0, 0, 0))  #make sure this is correct {'s': '0dda044ee7132f145c8fc0f73a3db5c6e5cf09422716eeddfe457b7741d432ba', 'r': '41e7d34d699902391c446cc4e98cac323f5a891c2b893bc666e240f8948e4327'}

if 13 == test:
	#for testing purposes
	zen.doAuth("accpt")
	zen.calcTxHash(tx1, 0, 1)
	print(zen.signTxn(0, 0, 0))
