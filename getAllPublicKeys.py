#!/usr/bin/env python
"""
*******************************************************************************
*   Ledger Blue
*   (c) 2016 Ledger
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
********************************************************************************
"""
from ledgerblue.comm import getDongle
from ledgerblue.commException import CommException
import argparse
import struct
from decimal import Decimal
import hashlib
import base58
import bech32
import sha3


apdu = "e008000000".decode('hex')

dongle = getDongle(True)
result = dongle.exchange(bytes(apdu))

def publicKeyIntoPublicAddress(data):
    publickey = result[0:33]
    s = hashlib.new('sha256', publickey).digest()
    r = hashlib.new('ripemd160', s).digest()
    return base58.b58encode(r)

def doSha(inp):
    h = sha3.sha3_256()
    h.update(inp)
    return list(ord(x) for x in h.digest())

print(list(result))

for i in xrange(0, 7):
    print "Address " + bech32.encode("zen",0, doSha(result[i*33:(i+1)*33]))
