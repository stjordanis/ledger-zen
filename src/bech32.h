
//The bech32 zen address encoding implementation for public keys
int bech32Encode(char *output, const char *hrp, int witver, const uint8_t *inpData, uint8_t inpDataLen);