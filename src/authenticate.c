#include <stdbool.h>
#include <os_io_seproxyhal.h>

#include "ux.h"
#include "retEnums.h"
#include "zen.h"

// This is the button handler for the approval screen
// It sends respond command and sets the ctx->isAuthenticated bool to the appropriate state
static unsigned int ui_authenticate_approve_button(unsigned int button_mask, unsigned int button_mask_counter) {
    switch (button_mask) {
        case BUTTON_EVT_RELEASED | BUTTON_LEFT: // REJECT
            G_io_apdu_buffer[0] = RET_VAL_REJECT_BUTTON;
            G_io_apdu_buffer[1] = 0x90;
            G_io_apdu_buffer[2] = 0x00;
            io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX , 3);
            globalAuthContext.isAuthenticated = false; //Just to be sure
            ui_idle();
            break;

        case BUTTON_EVT_RELEASED | BUTTON_RIGHT: // APPROVE
            G_io_apdu_buffer[0] = RET_VAL_SUCCESS;
            G_io_apdu_buffer[1] = 0x90;
            G_io_apdu_buffer[2] = 0x00;
            io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX , 3);
            globalAuthContext.isAuthenticated = true;
            ui_idle();
            break;
    }

    return 0;
}


//This is the autherization approval screen
static const bagl_element_t ui_authenticate_approve[] = {
        UI_BACKGROUND(),
        UI_ICON_LEFT(0x00, BAGL_GLYPH_ICON_CROSS),
        UI_ICON_RIGHT(0x00, BAGL_GLYPH_ICON_CHECK),
        UI_TEXT(0x00, 0, 12, 128, "Authenticate:"),
        UI_TEXT(0x00, 0, 26, 128, globalAuthContext.authCodeStr),
};


void debugSend(uint8_t p1, uint8_t p2, uint16_t p3, uint16_t p4, uint8_t* buffer, uint16_t bufferSize);

//Init state from authTxnHash, since we are switching apps, autherized txn hashs should be removed from state
void initState();

void handleAuth(uint8_t p1, uint8_t p2, uint8_t *dataBuffer, uint16_t dataLength,
        volatile unsigned int *flags, volatile unsigned int *tx) {

    globalAuthContext.isAuthenticated = false;
    initState();

    if (sizeof(globalAuthContext.authCodeStr) + sizeof(globalAuthContext.authKey) != dataLength) {
        G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_SIZE_ERR;
        G_io_apdu_buffer[(*tx)++] = 0x90;
        G_io_apdu_buffer[(*tx)++] = 0x00;
        return;
    }

    if (0 != dataBuffer[sizeof(globalAuthContext.authCodeStr) - 1]) {
        G_io_apdu_buffer[(*tx)++] = RET_VAL_WRONG_SIZE_AUTH_CODE;
        G_io_apdu_buffer[(*tx)++] = 0x90;
        G_io_apdu_buffer[(*tx)++] = 0x00;
        return;
    }

    os_memmove(globalAuthContext.authCodeStr, dataBuffer, sizeof(globalAuthContext.authCodeStr));
    os_memmove(globalAuthContext.authKey, dataBuffer + sizeof(globalAuthContext.authCodeStr), sizeof(globalAuthContext.authKey));

    UX_DISPLAY(ui_authenticate_approve, NULL);

    *flags |= IO_ASYNCH_REPLY;
}
