

typedef struct {
	uint8_t authCodeStr[6];
	uint8_t authKey[32];
	bool isAuthenticated;

} authenticateContext_t;

extern authenticateContext_t globalAuthContext;

//Derives zen keys
bool deriveZenKeypair(uint32_t index, uint32_t walletType, uint32_t accountNumber, 
                        cx_ecfp_private_key_t *out_privateKey, cx_ecfp_public_key_t *publicKey_out, uint16_t * outException);
