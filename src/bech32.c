
//This is the bech32 implementation we use to encode the public keys into zen addresses
//You need to call segwit_addr_encode to use it

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <os.h>



uint32_t bech32_polymod_step(uint32_t pre) {
    uint8_t b = pre >> 25;
    return ((pre & 0x1FFFFFF) << 5) ^ (-((b >> 0) & 1) & 0x3b6a57b2UL) ^
           (-((b >> 1) & 1) & 0x26508e6dUL) ^ (-((b >> 2) & 1) & 0x1ea119faUL) ^
           (-((b >> 3) & 1) & 0x3d4233ddUL) ^ (-((b >> 4) & 1) & 0x2a1462b3UL);
}

static const char *charset = "qpzry9x8gf2tvdw0s3jn54khce6mua7l";

int encodeStep2(char *output, const char *hrp, const uint8_t *data,
                  size_t data_len) {
    uint32_t chk = 1;
    size_t i = 0;
    while (hrp[i] != 0) {
        if (hrp[i] >= 'A' && hrp[i] <= 'Z')
            return 0;
        if (!(hrp[i] >> 5))
            return 0;
        chk = bech32_polymod_step(chk) ^ (hrp[i] >> 5);
        ++i;
    }

    chk = bech32_polymod_step(chk);
    while (*hrp != 0) {
        chk = bech32_polymod_step(chk) ^ (*hrp & 0x1f);
        *(output++) = *(hrp++);
    }
    *(output++) = '1';
    for (i = 0; i < data_len; ++i) {
        if (*data >> 5)
            return 0;
        chk = bech32_polymod_step(chk) ^ (*data);
        *(output++) = charset[*(data++)];
    }
    for (i = 0; i < 6; ++i) {
        chk = bech32_polymod_step(chk);
    }
    chk ^= 1;
    for (i = 0; i < 6; ++i) {
        *(output++) = charset[(chk >> ((5 - i) * 5)) & 0x1f];
    }
    *output = 0;
    return 1;
}

static int convert_bits(uint8_t *out, size_t *outlen, int outbits,
                        const uint8_t *in, size_t inlen, int inbits, int pad) {
    uint32_t val = 0;
    int bits = 0;
    uint32_t maxv = (((uint32_t)1) << outbits) - 1;
    while (inlen--) {
        val = (val << inbits) | *(in++);
        bits += inbits;
        while (bits >= outbits) {
            bits -= outbits;
            out[(*outlen)++] = (val >> bits) & maxv;
        }
    }
    if (pad) {
        if (bits) {
            out[(*outlen)++] = (val << (outbits - bits)) & maxv;
        }
    } else if (((val << (outbits - bits)) & maxv) || bits >= inbits) {
        return 0;
    }
    return 1;
}

int bech32Encode(char *output, const char *hrp, int witver, const uint8_t *inpData, uint8_t inpDataLen) {
    uint8_t data[180]; //Prob the size can be 90/5*8 = 144, but i don't want to take any chances or think about it...
    size_t datalen = 0;

    //max output len is 90 acording to spec
    if (inpDataLen + strlen(hrp) + 7 > 90)
        return 0;

    data[0] = witver;
    convert_bits(data + 1, &datalen, 5, inpData, inpDataLen, 8, 1);
    ++datalen;
    return encodeStep2(output, hrp, data, datalen);
}

