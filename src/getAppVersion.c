#include <stdint.h>
#include <stdbool.h>
#include <os.h>
#include <os_io_seproxyhal.h>

#define APP_VERSION 1

//It is what is says ^^
//todo: figure out what volatile means
void getAppVersionHandler(uint8_t p1, uint8_t p2, uint8_t *dataBuffer, uint16_t dataLength,
                            volatile unsigned int *flags, volatile unsigned int *tx) {
	
	uint8_t zenSig[] = {0x5a, 0x65, 0x6e, 0x32, 0x4d, 0x6f, 0x6f, 0x6e, 0x00}; //Easter egg

	G_io_apdu_buffer[(*tx)++] = APP_VERSION;

	os_memmove(G_io_apdu_buffer + 1, zenSig, sizeof(zenSig));
	*tx += sizeof(zenSig);

	G_io_apdu_buffer[(*tx)++] = 0x90;
	G_io_apdu_buffer[(*tx)++] = 0x00;
}
