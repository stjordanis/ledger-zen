#include <stdint.h>
#include <stdbool.h>
#include <os.h>
#include <os_io_seproxyhal.h>

#include "zen.h"
#include "ux.h"
#include "retEnums.h"


// Max buffer send back size is 255+5 bytes so
// the max number of keys i can send back is 8
#define NUM_KEYS_PER_CALL 7

//This converts a pub key into compact form or something like that
void extractPubkeyBytes(uint8_t *dst, cx_ecfp_public_key_t *publicKey) {
    //We are checking for parity here in order to compress the key
    *dst = (( publicKey->W[64] & 1) ? 0x03 : 0x02);
    os_memmove(dst + 1, publicKey->W + 1, 32);
}

// getAllPublicKeysHandlerHelper is the entry point for the getPublicKey command. It
// reads the command parameters, prepares and displays the approval screen
void getAllPublicKeysHandlerHelper(uint8_t p1, uint8_t p2, uint8_t *dataBuffer, uint16_t dataLength,
        volatile unsigned int *flags, volatile unsigned int *tx) {
    if (!globalAuthContext.isAuthenticated) {
        G_io_apdu_buffer[(*tx)++] = RET_VAL_NOT_AUTHENTICATED_ERR;
        return;
    }

    uint32_t index, coinType, accountNumber;

    if (dataLength != sizeof(globalAuthContext.authKey) + sizeof(index) + sizeof(coinType) + sizeof(accountNumber)) {
        G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_SIZE_ERR;
        return;
    }

    for (uint8_t i = 0; i < sizeof(globalAuthContext.authKey); i++) {
        if (dataBuffer[i] != globalAuthContext.authKey[i]) {
            G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_AUTH_KEY_ERR;
            return;
        }
    }


    os_memmove(&index, dataBuffer + sizeof(globalAuthContext.authKey), sizeof(index));
    os_memmove(&coinType, dataBuffer + sizeof(globalAuthContext.authKey) + sizeof(index), sizeof(coinType));
    os_memmove(&accountNumber, dataBuffer + sizeof(globalAuthContext.authKey) + sizeof(index) + sizeof(coinType), sizeof(accountNumber));

    cx_ecfp_public_key_t publicKey;

    G_io_apdu_buffer[(*tx)++] = RET_VAL_SUCCESS;

    for (uint8_t i = 0; i < NUM_KEYS_PER_CALL; i++) {
        uint16_t exception = 0;
        if (!deriveZenKeypair(index + i, coinType, accountNumber, NULL, &publicKey, &exception)) {
            G_io_apdu_buffer[0] = RET_VAL_KEY_DERIVE_EXCEPTION;
            G_io_apdu_buffer[1] = exception >> 8;
            G_io_apdu_buffer[2] = exception & 0xFF;
            *tx = 3;
            return;
        }

        extractPubkeyBytes(G_io_apdu_buffer + *tx, &publicKey);
        *tx += 33;
    }
}

// This is the getPublicKey command handler, it just adds calls getAllPublicKeysHandlerHelper
// and adds 0x9000 at the end of the return packet
void getAllPublicKeysHandler(uint8_t p1, uint8_t p2, uint8_t *dataBuffer, uint16_t dataLength,
                volatile unsigned int *flags, volatile unsigned int *tx) {

    getAllPublicKeysHandlerHelper(p1, p2, dataBuffer, dataLength, flags, tx);
    G_io_apdu_buffer[(*tx)++] = 0x90;
    G_io_apdu_buffer[(*tx)++] = 0x00;
}
