#include <stdbool.h>
#include <stdint.h>
#include <os.h>
#include <cx.h>
#include "zen.h"

//This function derives zen public and private keys
//Returns true iff a key was successfully derived
bool deriveZenKeypair(uint32_t index, uint32_t walletType, uint32_t accountNumber, 
                        cx_ecfp_private_key_t *out_privateKey, cx_ecfp_public_key_t *publicKey_out, uint16_t * outException) {
    
    //All of the called functions bellow throw upon error

    uint8_t keySeed[32];
    cx_ecfp_private_key_t pk;

    // bip32 path for zen is 44'/258
    // There is a config in the makefile that locks the path, so
    // if you want to change it you need to change the makefile
    // The 0x80000000 is there for ' <- sign, I don't know much about it except that
    uint32_t bip32Path[] = {44 | 0x80000000, 258 | 0x80000000, accountNumber | 0x80000000, walletType | 0x00000000, index};

    BEGIN_TRY {
            TRY {
                    os_perso_derive_node_bip32(CX_CURVE_256K1, bip32Path, 5, keySeed, NULL); // A void function

                    cx_ecfp_init_private_key(CX_CURVE_256K1, keySeed, sizeof(keySeed), &pk); // Return value has no meaning
                    if (publicKey_out) {
                        cx_ecfp_init_public_key(CX_CURVE_256K1, NULL, 0, publicKey_out); // Return value has no meaning
                        cx_ecfp_generate_pair(CX_CURVE_256K1, publicKey_out, &pk, 1); // Return value has no meaning
                    }
            } 
            CATCH_OTHER(e) {
                *outException = e;
                return false;      
            }
            FINALLY {
            }
        }
        END_TRY;


    if (out_privateKey) {
        *out_privateKey = pk;
    }
    os_memset(keySeed, 0, sizeof(keySeed));
    os_memset(&pk, 0, sizeof(pk));

    return true;
}